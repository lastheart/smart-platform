# 1 开发设置向导

## 1.1 环境需求
- nacos
- RocketMQ
- mysql

## 1.2 添加仓库地址 
```
<profile>
    <id>rdc-private-repo</id>
    <repositories>
        <repository>
            <id>rdc-releases</id>
            <url>https://repo.rdc.aliyun.com/repository/2451-release-8qdhKz/</url>
        </repository>
        <repository>
            <id>rdc-snapshots</id>
            <url>https://repo.rdc.aliyun.com/repository/2451-snapshot-kGepUG/</url>
        </repository>
    </repositories>
</profile>
```
