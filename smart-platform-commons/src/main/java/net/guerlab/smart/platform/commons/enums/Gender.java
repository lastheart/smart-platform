package net.guerlab.smart.platform.commons.enums;

/**
 * 性别
 *
 * @author guer
 */
@SuppressWarnings("unused")
public enum Gender {

    /**
     * 男
     */
    MAN,

    /**
     * 女
     */
    WOMAN,

    /**
     * 其他
     */
    OTHER
}
