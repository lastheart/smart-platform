package net.guerlab.smart.platform.commons;

/**
 * 常量
 *
 * @author guer
 */
public interface Constants {

    /**
     * 默认上级ID
     */
    Long DEFAULT_PARENT_ID = 0L;

    /**
     * 默认排序值
     */
    Integer DEFAULT_ORDER_NUM = 0;

    /**
     * TOKEN
     */
    String TOKEN = "Authorization";

    /**
     * 空ID
     */
    Long EMPTY_ID = 0L;

    /**
     * 空名称
     */
    String EMPTY_NAME = "";

}
