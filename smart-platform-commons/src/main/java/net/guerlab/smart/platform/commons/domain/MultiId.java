package net.guerlab.smart.platform.commons.domain;

import java.util.ArrayList;

/**
 * ID集合
 *
 * @author guer
 */
public class MultiId extends ArrayList<Long> {}
